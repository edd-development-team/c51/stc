# EDD-STC #

***License: Apache-2.0***

## 代码贡献 ##

* 贡献流程请参阅contribution.md

* 代码风格请参阅coding_style.md

## 下阶段开发计划 ##

- 分离驱动逻辑层和硬件接口, 统一bus接口
- 完善驱动，添加nrf，12864驱动；添加stc12c5a60s2片上SPI、EEPROM驱动
- 优化 CI
- 完善 demo；添加整板测试例程
- 添加应用示例
- 优化文件结构
