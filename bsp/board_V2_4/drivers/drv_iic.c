/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file board.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning none
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-03-04  |   embed       |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
#include "drv_delay.h"
#include "drv_iic.h"
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
sbit I2C_SCL = P3^6;
sbit I2C_SDA = P3^7;
/* Private define --------------------------------------------------------------------------*/
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/

/**
  * @brief iic communication start
  * @param roll[in]: none
  * @return none
  */
void iic_start(void)
{
    /* Init iic bus position */
    I2C_SDA = 1; 
    I2C_SCL = 1; 
    delay_us(1);      

    /* Pull down SDA and SCL to communication */
    I2C_SDA = 0;
    delay_us(1);      	
    I2C_SCL = 0; 
    delay_us(1);                   
}


/**
  * @brief iic communication stop
  * @param roll[in]: none
  * @return none
  */
void iic_stop(void)
{
    /* Init iic bus position */
    I2C_SCL = 0;
    I2C_SDA = 0;
    delay_us(1);      

    /* Pull up SDA and SCL to stop communication */
    I2C_SCL = 1;
    delay_us(1);                  
    I2C_SCL = 1; 
    delay_us(1);               
}


/**
  * @brief iic write data 
  * @param roll[in]: we_data "data which is input"
  * @return action
  */
bool iic_write(const uint8_t wr_data)
{
    uint8_t i;
    bool ack;

    for(i=0; i<8; i++)
    {
        /* write data from high bool to low bool */
        I2C_SDA = wr_data & (0x80>>i);	
        /* change SCL with delay to communication */
        delay_us(3);
        I2C_SCL = 1;              
        delay_us(3); 
        I2C_SCL = 0;               
    }

    /* free SDA to read ack */
    I2C_SDA = 1; 
    delay_us(1);
    I2C_SCL = 1; 
    ack = I2C_SDA; 
    delay_us(1); 
    I2C_SCL = 0;  

    return (ack);
}

/**
  * @brief iic read data 
  * @param roll[in]: ack "action"
  * @return read data
  */
uint8_t iic_read(const bool ack)
{
    uint8_t i;
    uint8_t re_data = 0x00;

    I2C_SDA = 1;	///< free SDA bus

    /* order clock to read data */
    for(i=0; i<8; i++)
    {
        I2C_SCL = 1; 
        delay_us(3);
        re_data |= (I2C_SDA ? (0x80>>i): 0x00);
        I2C_SCL = 0; 
        delay_us(3);
    }

    I2C_SDA = ack;                            
    delay_us(1);
    I2C_SCL = 1;                 
    delay_us(1); 
    I2C_SCL = 0;                   

    return (re_data);
}

/**
  * @}
  */
/************************ (C) COPYRIGHT GYC *****END OF FILE*********************************/
