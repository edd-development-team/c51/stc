/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_key.c
  * @version 0.2.2
  * @brief none
  * @details none
  * @warning standard input and output library functions are not recommended. (such as printf and scanf fuctions in <stdio.h>)
  * @bug none
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-3-12   |    YC         |   the third version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "drv_key.h"
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
#define KEY_PORT        P2
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
key_flags_st key_flags;
key_value_st key_value;
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/

/**
  * @brief Key matrix scan, when the key is pressed, key flag bit is set
  * @param[out] key_flag: key flags address pointer
  * @bug Other peripherals that affect the same port
  */
void keyMatrixScan(void)
{
    uint8_t count = 0;
    uint16_t temp = 0;
    static uint16_t store = 0;

    for (count = 0; count < 4; count++)                  ///< Line up the key flags.
    {
        temp >>= 4;
        KEY_PORT = ~(0x80 >> count);
        temp |= (KEY_PORT & 0x0f) << 12;
    }
    temp = ~temp;
    *((uint16_t*)(&key_value)) = temp;                          ///< Store current key values.
    *((uint16_t*)(&key_flags)) |= temp & (store ^ temp);        ///< The rising edge changes the key flags.
    store = temp;                                               ///< Store the last key flag status.
}

/**
  * @brief Single key scan, when the key is released, key flag bit is set
  * @param[out] key_flag: key flags address pointer
  * @bug None
  */
void keySingleScan(key_numbers_en key_number)
{
    uint8_t temp = 0;
    static uint8_t store = 1;

    KEY_PORT &= ~(0x80 >> (key_number >> 2));           ///< The corresponding pins are pulled down.
    KEY_PORT |= 0x01 << (key_number % 4);               ///< The corresponding pins are pulled up.
    temp = (KEY_PORT >> (key_number % 4)) & 0x01;       ///< Get key status.
    *((uint16_t*)(&key_value)) = temp;                  ///< Store current key values.
    *((uint16_t*)(&key_flags)) |= ((temp & (store ^ temp)) << key_number);      ///< Set key flags.
    store = temp;                                       ///< Store the last key flag status.
}

/**
  * @}
  */
/************************ (C) COPYRIGHT YC *****END OF FILE*********************************/
