/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_key.h
  * @version 0.2.1
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-3-12   |    YC         |   the second version
***/

/* Define to prevent recursive inclusion ---------------------------------------------------*/
#ifndef __DRV_KEY_H__
#define __DRV_KEY_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
#include <stdio.h>
/* Exported types --------------------------------------------------------------------------*/
/**
* Key number enumeration.
*/
typedef enum
{
    KEY_1 = 0,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,
    KEY_10,
    KEY_11,
    KEY_12,
    KEY_13,
    KEY_14,
    KEY_15,
    KEY_16,
} key_numbers_en;
/**
* Key segment structure.
*/
typedef struct
{
    uint16_t key_1_flag:  1;
    uint16_t key_2_flag:  1;
    uint16_t key_3_flag:  1;
    uint16_t key_4_flag:  1;
    uint16_t key_5_flag:  1;
    uint16_t key_6_flag:  1;
    uint16_t key_7_flag:  1;
    uint16_t key_8_flag:  1;
    uint16_t key_9_flag:  1;
    uint16_t key_10_flag: 1;
    uint16_t key_11_flag: 1;
    uint16_t key_12_flag: 1;
    uint16_t key_13_flag: 1;
    uint16_t key_14_flag: 1;
    uint16_t key_15_flag: 1;
    uint16_t key_16_flag: 1;
} key_flags_st;

/**
* Key values structure.
*/
typedef struct
{
    uint16_t key_1_value:  1;
    uint16_t key_2_value:  1;
    uint16_t key_3_value:  1;
    uint16_t key_4_value:  1;
    uint16_t key_5_value:  1;
    uint16_t key_6_value:  1;
    uint16_t key_7_value:  1;
    uint16_t key_8_value:  1;
    uint16_t key_9_value:  1;
    uint16_t key_10_value: 1;
    uint16_t key_11_value: 1;
    uint16_t key_12_value: 1;
    uint16_t key_13_value: 1;
    uint16_t key_14_value: 1;
    uint16_t key_15_value: 1;
    uint16_t key_16_value: 1;
} key_value_st;

/* Exported constants ----------------------------------------------------------------------*/
/* Exported macro --------------------------------------------------------------------------*/
/* Exported variables ----------------------------------------------------------------------*/
extern key_flags_st     key_flags;          ///< Store key press flags.
extern key_value_st     key_value;          ///< Store key current value.
/* Exported functions ----------------------------------------------------------------------*/
void keyMatrixScan(void);
void keySingleScan(key_numbers_en key_number);
#ifdef __cplusplus
}
#endif

#endif

/************************ (C) COPYRIGHT YC *****END OF FILE*********************************/
