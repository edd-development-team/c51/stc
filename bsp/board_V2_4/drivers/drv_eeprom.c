/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file board.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning none
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-03-04  |   embed       |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
#include "drv_iic.h"
#include "drv_eeprom.h"
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/

/**
  * @brief eeprom write data 
  * @param roll[in]: buf "data"
  * @param roll[in]: addr "address"
  * @param roll[in]: len  "lenth"
  * @return 0x11 "eeprom is not found"
			0x21 "write addr unable"
			0x22 "writa addr unable"
			0x00 "normal return"
  */
uint8_t eeprom_write(uint8_t *buf, uint8_t addr, uint8_t len)
{
    uint8_t i = 0;

    while(len > 0)
    {
        while(1)
        {
            /* connect eeprom if overtime break */
            iic_start();
            if(0 == iic_write(0xA2)) 
                break;    		///<success
            iic_stop();
            if (i++ > 100) 
                return 0x11;   	///<fault
        }
        /* write address */
        if(iic_write(addr)) 
            return 0x21;     	///<fault

        while (len > 0)
        {
            /* write data */
            if(iic_write(*buf++)) 
                return 0x22;  	///<fault
            len--;
            addr++;
            if ((addr&0x07) == 0) 
                break;       	///<next page
        }
        iic_stop();              ///<stop current page
    }

    return 0x00;                ///<all write is success
}

/**
  * @brief eeprom read data 
  * @param roll[in]: buf "data"
  * @param roll[in]: addr "address"
  * @param roll[in]: len  "lenth"
  * @return 0x11 "eeprom is not found"
			0x21 "write addr unable"
			0x22 "writa addr unable"
			0x00 "normal return"
  */
uint8_t eeprom_read(uint8_t *buf, uint8_t addr, uint8_t len)
{                 
    uint8_t i = 0;

    while(1)
    {
        /* connect eeprom if overtime break */
        iic_start();
        if (0 == iic_write(0xA2)) 
            break;         		///<success
        iic_stop();
        if (i++ > 100) 
            return 0x11;        ///<fault
    }
    if(iic_write(addr)) 
        return 0x21;            ///<fault

    /* Restart bus to read */
    iic_start();
    if(iic_write(0xA3)) 
        return 0x22;            ///<fault

    while(--len) 
        *buf++ = iic_read(0);    
    *buf = iic_read(1);          ///<no ack read

    iic_stop();

    return 0x00;                ///<read is success
}

/**
  * @}
  */
/************************ (C) COPYRIGHT GYC *****END OF FILE*********************************/
