/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_oled.h
  * @version 0.1.0
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-3-15   |    YC         |   the first version
***/

/* Define to prevent recursive inclusion ---------------------------------------------------*/
#ifndef __DRV_OLED_H__
#define __DRV_OLED_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
/* Exported types --------------------------------------------------------------------------*/
/* Exported constants ----------------------------------------------------------------------*/
/* Exported macro --------------------------------------------------------------------------*/
#define OLED_PRINT   (1)          ///< use oled printf
/* Exported functions ----------------------------------------------------------------------*/
void init_oled(void);
void oledSetPos(uint8_t x, uint8_t y);
void oledFill(uint8_t bmp_dat);
void oledCls(void);
void oledPlay_6x8str(uint8_t x, uint8_t y, uint8_t ch[]);
void oledPlay_8x16str(uint8_t x, uint8_t y, uint8_t ch[]);
void oledPlay_16x16char(uint8_t x, uint8_t y, uint8_t n);
void oledDrawBmp(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t bmp[]);
void oledShowCross(uint8_t *table, uint8_t length, uint8_t width);
void oledDrawPoint(uint8_t x, uint8_t y, bit cmd, uint8_t *p);
void oledCleanPoint(uint8_t *p);

#if OLED_PRINT
void oledPrintChar(uint8_t ch);
void printSetLine(uint8_t line, uint8_t xaxis);
#endif


#ifdef __cplusplus
}
#endif

#endif

/************************ (C) COPYRIGHT YC *****END OF FILE*********************************/
