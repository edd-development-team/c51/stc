/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_infrared.h
  * @version 0.1.0
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-2-19   |    YC         |   the first version
***/

/* Define to prevent recursive inclusion ---------------------------------------------------*/
#ifndef __DRV_INFRARED_H__
#define __DRV_INFRARED_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
/* Exported types --------------------------------------------------------------------------*/
typedef struct
{
    uint8_t     ir_user_code;
    uint8_t     ir_user_rcode;
    uint8_t     ir_data;
    uint8_t     ir_rdata;
    uint8_t     ir_flag;
} ir_data_st;
/* Exported constants ----------------------------------------------------------------------*/
/* Exported macro --------------------------------------------------------------------------*/
/* Exported variables ----------------------------------------------------------------------*/
extern ir_data_st ir_receive;
extern ir_data_st ir_send;
/* Exported functions ----------------------------------------------------------------------*/
void infrared_init(void);
void sendInfraredData(uint16_t address, uint8_t dat, ir_data_st* ir_sen);
#ifdef __cplusplus
}
#endif

#endif

/************************ (C) COPYRIGHT GYC *****END OF FILE*********************************/
