/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_NUM_LED.h
  * @version 0.1.0	
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-1-22   |   WS          |   the first version
***/

/* Define to prevent recursive inclusion ---------------------------------------------------*/
#ifndef __DRV_DIGITYON_H__
#define __DRV_DIGITYON_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
#include "drv_led.h"   
/* Exported types --------------------------------------------------------------------------*/
/* Exported constants ----------------------------------------------------------------------*/
/* Exported macro --------------------------------------------------------------------------*/     
/* Exported functions ----------------------------------------------------------------------*/  
void digitron_init(void);
void digitron_showScan(void);
void digitron_showNum(uint32_t longnum);
void digitron_showFloatNum(float floatnum);
#ifdef __cplusplus
}
#endif

#endif

/************************ (C) COPYRIGHT GYC *****END OF FILE*********************************/
