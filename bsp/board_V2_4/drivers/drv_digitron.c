/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_num_led.c
  * @version 0.1.0
  * @brief none
  * @details none
  * @warning none
  * @bug none
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-1-22   |   WS          |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "drv_digitron.h"
#include <stdio.h>
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
code uint8_t num_table[] = {0XC0, 0XF9, 0XA4, 0XB0, 0X99, 0X92, 0X82, 0XF8, 0X80, 0X90,0XFF};
volatile uint8_t num_buff[7];
sbit DIGITRON_EN_RB = P1^1;
uint8_t floatNum_buff[6];
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/
/**
  * @brief Initialize NUM_LED
  * @bug
  */
void digitron_init(void)
{
    uint8_t i;
    DIGITRON_EN_RB = 0X00;
    P0 = 0XFF;
    for (i = 0; i < 7; i++)
    {
        num_buff[i] = 0XFF;
    }
}
/**
  * @brief Nixie tube scanning function
           Suggest putting it in timer interrupts
  */
void digitron_showScan(void)
{
    static uint8_t num;
    
    P0 = 0XFF;
    DIGITRON_EN_RB = 1;
    P2 &= ~(7 << 0);
    P2 |= (num % 7 << 0);
    P0 = num_buff[(num++) % 7];
    DIGITRON_EN_RB = 0;
}
/**
  * @brief  Digital display function
  * @param[in] longnum: Data to display
  * @bug Floating point or character is not supported
  */
void digitron_showNum(uint32_t longnum)
{
    uint8_t n = 6;
    
    while (n)
    {
        num_buff[n - 1] = num_table[longnum % 10];
        longnum /= 10;
        n--;
        if (longnum == 0)
        {
            num_buff[n - 1] = 0XFF;///<Remove the previous bit
            break;
        }
    }
    num_buff[6] = P0_BUFF;
}
/**
  * @brief  Digital Tube Displays Floating Point Data
  * @param[in] floatnum: Data to display
  * @bug none
  */

void digitron_showFloatNum(float floatnum)
{
    uint8_t n = 6;
    uint8_t i = 6;
    
    sprintf((char*)floatNum_buff,"%6f",floatnum);
    while (n)
    {
        num_buff[n - 1] = num_table[floatNum_buff[i] - '0'];
        if (floatNum_buff[i] == '.')
        {
            num_buff[n - 1] = num_table[floatNum_buff[i - 1] - '0'] & 0x7f;///<Add decimal points
            i--;
        }
        n--;
        i--;
    }
    num_buff[6] = P0_BUFF;
}
/**
  * @}
  */
/************************ (C) COPYRIGHT WS  *****END OF FILE*********************************/
